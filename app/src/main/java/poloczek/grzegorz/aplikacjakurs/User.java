package poloczek.grzegorz.aplikacjakurs;

import java.io.Serializable;

/**
 * Created by RENT on 2017-06-06.
 */

public class User implements Serializable{
    private String firstName;
    private String lastName;
    private boolean additionalMaterials;

    public User(String firstName, String lastName, boolean additionalMaterials) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.additionalMaterials = additionalMaterials;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {return lastName;}

    public boolean isAdditionalMaterials() {
        return additionalMaterials;
    }
}
