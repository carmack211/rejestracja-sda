package poloczek.grzegorz.aplikacjakurs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageActivity extends AppCompatActivity {

    private int PICK_IMAGE_REQUEST = 1;
    private int MAKE_IMAGE_REQUEST = 2;
    private int STORAGE_PERMISSION_CODE = 123;
    private String pathPhoto = "";
    static final String IMAGE = "image";

    @BindView(R.id.recivedImageView)
    ImageView kontrolkaZdjecia;

    @OnClick(R.id.floatingActionButtonImage)
    public void wczytajZdjecie(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ImageActivity.this);
        alertDialog.setTitle("Select option");

        final CharSequence[] items = {"Take a picture" , "Choose from gallery", "Load from internet"};

        alertDialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0){
                    takePicture();
                }else if(which == 1){
                    chooseFromGallery();
                }else if(which == 2){
                    loadFromInternet();
                }
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog al = alertDialog.create();
        al.show();


    }

    public void loadFromInternet(){
           Glide.with(getApplicationContext()).
                   load("https://betaimages.sunfrogshirts.com/2016/04/01/m_Awesome-Tee-For-Junior-Android-Developer-Red-_w91_-front.jpg").
                   into(kontrolkaZdjecia);
    }



    public void chooseFromGallery(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showExplanation();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
            }
            return;
        }else{
            loadGallery();
        }
    }

    private void showExplanation() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle("Permission needed")
                .setMessage("We need access to the gallery")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActivityCompat.requestPermissions(ImageActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                    }
                });
        builder.create().show();
    }
    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                chooseFromGallery();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Nie będziemy mogli wykonać tej operacji", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void loadGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //intent.setType("image/*");
        //intent.setAction(Intent.ACTION_GET_CONTENT);
        //startActivityForResult(Intent.createChooser(intent, "Choose photo"), PICK_IMAGE_REQUEST);
        startActivityForResult(intent, PICK_IMAGE_REQUEST );
    }

    public void takePicture(){
        Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePhoto.resolveActivity(getPackageManager()) != null){
            startActivityForResult(takePhoto, MAKE_IMAGE_REQUEST);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        ButterKnife.bind(this);

        if(savedInstanceState != null){
            pathPhoto = savedInstanceState.getString(IMAGE);
            Glide.with(getApplicationContext()).load(pathPhoto).into(kontrolkaZdjecia);
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode != Activity.RESULT_CANCELED) {
            if (requestCode == PICK_IMAGE_REQUEST) {
                Uri filePath = data.getData();
                String log2 = getRealPathFromUri(getApplicationContext(), filePath);
                pathPhoto = log2;
                Glide.with(getApplicationContext()).load("file://"+ pathPhoto).into(kontrolkaZdjecia);

            } else if(requestCode == MAKE_IMAGE_REQUEST){
                String photoPath = "";

                Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_ADDED, MediaStore.Images.ImageColumns.ORIENTATION}, MediaStore.Images.Media.DATE_ADDED, null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");
                if (cursor != null && cursor.moveToFirst()) {

                    cursor.moveToFirst();
                    Uri uri = Uri.parse(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA)));
                    photoPath = uri.toString();
                    cursor.close();
                }
                pathPhoto = photoPath;
                Glide.with(getApplicationContext()).load("file://"+ pathPhoto).into(kontrolkaZdjecia);

            }
        }

    }



    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(IMAGE, pathPhoto);
        super.onSaveInstanceState(outState);
    }
}
