package poloczek.grzegorz.aplikacjakurs;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.File;

public class ServiceActivity extends AppCompatActivity {

    public static final String MY_PREFS = "KURS";
    public static final String MUZYKA = "kursMusicKey";
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        sharedPreferences = getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE);
    }

    public void startClick(View view) {
        Intent i = new Intent(ServiceActivity.this, ServiceClass.class);
        //  i.putExtra("Powiadomienie","Powiadomienie");
        ServiceActivity.this.startService(i);

        if(isConnected(getApplicationContext()) == true){
            sharedPreferences.edit().putBoolean(MUZYKA, true).apply();
            Intent j = new Intent(ServiceActivity.this, MusicServiceClass.class);
            startService(j);
        }

    }

    public void stopClick(View view) {
        Intent i = new Intent(ServiceActivity.this, ServiceClass.class);
        ServiceActivity.this.stopService(i);
        Toast.makeText(getApplicationContext(), "ServiceDeactivated", Toast.LENGTH_SHORT).show();

        sharedPreferences.edit().putBoolean(MUZYKA, false).apply();
        Intent j = new Intent(ServiceActivity.this, MusicServiceClass.class);
        stopService(j);
    }

    public static boolean isConnected(Context context) {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        return plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB;
    }
}
