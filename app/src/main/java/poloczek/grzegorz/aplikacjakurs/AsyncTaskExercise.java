package poloczek.grzegorz.aplikacjakurs;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by RENT on 2017-07-25.
 */

public class AsyncTaskExercise extends AsyncTask<Integer, Integer, Integer> {

    private TextView textView;
    private AppCompatActivity activity;
    private ProgressDialog dialog;

    public AsyncTaskExercise(TextView textView, AppCompatActivity activity) {
        this.textView = textView;
        this.activity = activity;

        dialog = new ProgressDialog(activity);
        dialog.setIndeterminate(true);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        textView.setText("Let's begin!");

        this.dialog.setMessage("Progress start");
        this.dialog.show();
    }

    @Override
    protected Integer doInBackground(Integer... params) {
        Integer value = params[0];
        for(int i=value; i>=0; i--){
            try {
                Thread.sleep(1000);
                publishProgress((int) i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        Integer value = values[0];
        textView.setText(value.toString());
        dialog.setMessage("Please wait .. " + value.toString());
    }

    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
        textView.setText("Zakończono odliczanie");

        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

}
