package poloczek.grzegorz.aplikacjakurs;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by RENT on 2017-07-17.
 */

public class SMSAdapter extends RecyclerView.Adapter<SMSAdapter.MyViewHolder>  {

    private List<TelephoneNumber> numberList;

    public SMSAdapter(List<TelephoneNumber> numberList) {
        this.numberList = numberList;
    }

    @Override
    public SMSAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.line_call_dialog,parent,false);
        return new SMSAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SMSAdapter.MyViewHolder holder, int position) {
        TelephoneNumber numberObject = numberList.get(position);
        holder.lineTelephoneNumber.setText(numberObject.getTelephoneNumber());


    }

    @Override
    public int getItemCount() {
        return numberList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView lineTelephoneNumber;


        public MyViewHolder(View itemView) {
            super(itemView);
            lineTelephoneNumber = (TextView) itemView.findViewById(R.id.lineTelephoneNumber);

        }
    }
}
