package poloczek.grzegorz.aplikacjakurs;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class AsyncTaskActivity extends AppCompatActivity {

    private TextView textView;
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;
    private Handler handler;
    private Handler handler2;
    private ProgressDialog progressDialog;
    private Handler updateBarHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task);
        textView = (TextView) findViewById(R.id.textViewAsync);

        updateBarHandler = new Handler();
    }

    public void asyncExecute(View view) {
        new AsyncTaskExercise(textView, this).execute(10);
    }

    public void pendingExecute(View view) {
        Intent i = new Intent(this, PanstwaActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, i, PendingIntent.FLAG_UPDATE_CURRENT);

        try {
            pendingIntent.send();
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }
    }

    public void setAlarm(View view) {
        //okreslamy opis intentu
        Intent i = new Intent(this, AlarmReciver.class);
        //PendingIntent.get .. zalezne od przekazywanego intentu
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, i, 0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 3000, pendingIntent);
    }

    public void lotteryNumber(View view) {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                textView.setText(msg.obj.toString());

            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                Random value = new Random();
                int randomValue = value.nextInt(100);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Message message = new Message();
                message.obj = randomValue;
                handler.sendMessage(message);
            }
        }).start();

        //handler2 "kasuje" widok z wylosowaną liczbą


        handler2 = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                textView.setText(msg.obj.toString());
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Message message = new Message();
                message.obj = "";
                handler.sendMessage(message);
            }
        }).start();
    }

    public void lotteryNumberProgressingBar(View view) {
        progressDialog = new ProgressDialog(AsyncTaskActivity.this);
        progressDialog.setTitle("Losowanie");
        progressDialog.setMessage("Następuje loswanie liczby");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setProgress(10);
        progressDialog.setMax(10);
        progressDialog.show();


        //https://examples.javacodegeeks.com/android/core/ui/progressdialog/android-progressdialog-example/

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Random value = new Random();
                    final int randomValue = value.nextInt(100);
                    while (progressDialog.getProgress() <= progressDialog.getMax()) {
                        Thread.sleep(1000);
                        updateBarHandler.post(new Runnable() {
                            public void run() {
                                progressDialog.incrementProgressBy(1);
                            }
                        });
                        if (progressDialog.getProgress() == progressDialog.getMax()) {
                            progressDialog.dismiss();
                            textView.post(new Runnable() {
                                @Override
                                public void run() {
                                    textView.setText(String.valueOf(randomValue));
                                }
                            });
                        }
                    }
                } catch (Exception e) {
                }
            }
        }).start();
    }
}
