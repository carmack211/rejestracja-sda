package poloczek.grzegorz.aplikacjakurs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import poloczek.grzegorz.aplikacjakurs.data.DbQueries;

public class PanstwaActivity extends AppCompatActivity {
    //@BindView(R.id.countryListView) ListView countryListView;
    //@BindView(R.id.countryTextView) TextView countryTextView;
    //ArrayAdapter<String> adapter;
    //public static final String TAG = "Panstwa";

    DbQueries dbQueries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panstwa);
        ButterKnife.bind(this);
        List<Country> countryList = new ArrayList<>();

        Country poland = new Country(R.drawable.poland, "Polska", "Warszawa");
        Country germany = new Country(R.drawable.germany, "Niemcy", "Berlin");
        Country england = new Country(R.drawable.england, "Anglia", "Londyn");
        Country france = new Country(R.drawable.france, "Francja", "Paryż");
        Country czechRepublic = new Country(R.drawable.czech_republic, "Czechy", "Praga");
        Country belgium = new Country(R.drawable.belgium, "Belgia", "Bruksela");
        Country italy = new Country(R.drawable.italy, "Włochy", "Rzym");
        Country austria = new Country(R.drawable.austria, "Austria", "Wiedeń");
        Country norway = new Country(R.drawable.norway, "Norwegia", "Oslo");

        countryList.add(poland);
        countryList.add(germany);
        countryList.add(england);
        countryList.add(france);
        countryList.add(czechRepublic);
        countryList.add(belgium);
        countryList.add(italy);
        countryList.add(austria);
        countryList.add(norway);

        RecyclerView myRecycler = (RecyclerView) findViewById(R.id.myRecyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        myRecycler.setLayoutManager(linearLayoutManager);

        CountryAdapter countryAdapter = new CountryAdapter(countryList);

        myRecycler.setAdapter(countryAdapter);

//        String[] country = {"Polska", "Włochy", "Argentyna", "Brazylia", "Francja", "Niemcy",
//                "Hiszpania", "Belgia", "Holandia", "Portugalia", "Nowa Zelandia", "Kolumbia",
//                "Egipt", "Maroko", "Rosja", "Chiny"};
//        String text = "";
//        for(String item : country){
//            text = text + item + "<br/>";
//        }
//
//        countryTextView.setText(fromHtml(text));


//        ArrayList<String> countryToList = new ArrayList<>();
//        countryToList.addAll(Arrays.asList(country));
//        adapter = new ArrayAdapter<>(this, R.layout.row, countryToList);
//        countryListView.setAdapter(adapter);

//        countryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String country = (String)parent.getItemAtPosition(position);
//                Log.d(TAG, country);
//            }
//        });
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html){
               Spanned result;
               if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
                    } else {
                   result = Html.fromHtml(html);
                    }
               return result;
          }

}
