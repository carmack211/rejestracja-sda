package poloczek.grzegorz.aplikacjakurs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;

public class RejestracjaActivity extends AppCompatActivity {
    AutoCompleteTextView autoCompleteTextView;
    CheckBox acceptanceCheckBox;
    EditText nameEditText;
    EditText lastNameEditText;
    Switch interestsSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejestracja);
        nameEditText = (EditText) findViewById(R.id.imieEditText);
        lastNameEditText = (EditText) findViewById(R.id.nazwiskoEditText);
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.countryText);
        acceptanceCheckBox = (CheckBox) findViewById(R.id.acceptanceCheck);
        interestsSwitch = (Switch) findViewById(R.id.interestsSwitch);
        final EditText telephoneNumber = (EditText) findViewById(R.id.telefonText);
        final EditText zipCode = (EditText) findViewById(R.id.zipCodeText);

        zipCode.setFilters(new InputFilter[] { new InputFilter.LengthFilter(6) });
        telephoneNumber.setFilters(new InputFilter[] {new InputFilter.LengthFilter(9) });

        countries();

        zipCode.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    zipCode.setText("");
                }
                return false;
            }
        });

        zipCode.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 2 ){
                    String convert = s + "-";
                    zipCode.setText(convert);
                    zipCode.setSelection(3);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });
    }

    public void countries(){
        String[] country = {"Polska", "Włochy", "Argentyna", "Brazylia", "Francja", "Niemcy",
                "Hiszpania", "Belgia", "Holandia", "Portugalia", "Nowa Zelandia", "Kolumbia",
                "Egipt", "Maroko", "Rosja", "Chiny"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, country );
        autoCompleteTextView.setAdapter(adapter);
    }

    public void registerValidation(View view) {
        final TextInputLayout nameWrapper = (TextInputLayout) findViewById(R.id.LayoutName);
        final TextInputLayout surnameWrapper = (TextInputLayout) findViewById(R.id.LayoutSurname);
        final TextInputLayout zipcodeWrapper = (TextInputLayout) findViewById(R.id.LayoutTelephone);
        final TextInputLayout phoneWrapper = (TextInputLayout) findViewById(R.id.LayoutZipCode);
        final TextInputLayout loginWrapper = (TextInputLayout) findViewById(R.id.LayoutLogin);
        final TextInputLayout passwordWrapper = (TextInputLayout) findViewById(R.id.LayoutPassword);
        final TextInputLayout adressWrapper = (TextInputLayout) findViewById(R.id.layoutAdress);
        //final CheckBox acceptanceWrapper = (CheckBox) findViewById(R.id.acceptanceCheck);
        String login = loginWrapper.getEditText().getText().toString();
        String password = passwordWrapper.getEditText().getText().toString();
        String name = nameWrapper.getEditText().getText().toString();
        String surname = surnameWrapper.getEditText().getText().toString();
        String zipCode = zipcodeWrapper.getEditText().getText().toString();
        String phone = phoneWrapper.getEditText().getText().toString();
        String adress = adressWrapper.getEditText().getText().toString();

        if (login.length() == 0) {
            loginWrapper.setError("Należy wypełnić to pole");
        } else{
            loginWrapper.setErrorEnabled(false);
        }

        if (password.length() == 0) {
            passwordWrapper.setError("Należy wypełnić to pole");
        } else{
            passwordWrapper.setErrorEnabled(false);
        }

        if (name.length() == 0) {
            nameWrapper.setError("Należy wypełnić to pole");
        } else{
            nameWrapper.setErrorEnabled(false);
        }

        if (surname.length() == 0) {
            surnameWrapper.setError("Należy wypełnić to pole");
        } else{
            surnameWrapper.setErrorEnabled(false);
        }

        if (adress.length() == 0) {
            adressWrapper.setError("Należy wypełnić to pole");
        } else{
            adressWrapper.setErrorEnabled(false);
        }

        if (zipCode.length() == 0) {
            zipcodeWrapper.setError("Należy wypełnić to pole");
        } else{
            zipcodeWrapper.setErrorEnabled(false);
        }

        if (phone.length() == 0) {
            phoneWrapper.setError("Należy wypełnić to pole");
        }  else {
            phoneWrapper.setErrorEnabled(false);

        }

        //tworzymy preferencję, do zapisywania danych w pamięci telefonu
        //za pomocą metody getSharedPrefereces, tworzy się plik xml o nazwie logins, ktora przechowa
        //login i haslo wpisane przez uzytkownika w widoku rejestracji
        SharedPreferences sharedLogins = getSharedPreferences("logins", Context.MODE_PRIVATE);
        SharedPreferences.Editor edtiorLogin = sharedLogins.edit();
        edtiorLogin.putString(getString(R.string.PREF_LOGIN), login);
        edtiorLogin.putString(getString(R.string.PREF_PASSWORD), password);
        edtiorLogin.apply();


        SharedPreferences sharedAdress = getSharedPreferences("adresses", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorAdress = sharedAdress.edit();
        editorAdress.putString(getString(R.string.PREF_ADRESS), adress);
        editorAdress.putString(getString(R.string.PREF_ZIPCODE), zipCode);
        editorAdress.apply();

    if (!acceptanceCheckBox.isChecked()) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RejestracjaActivity.this);
        alertDialogBuilder.setTitle("Wymagana akceptacja regulaminu")
                .setMessage("Aby zakończyć rejestrację musisz zaakceptować regulamin")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }
    if(!(login.length() == 0 && password.length() == 0 && name.length() == 0 &&
        surname.length() == 0 && zipCode.length() == 0 && phone.length() == 0
            && !acceptanceCheckBox.isChecked())){
            Intent intent = new Intent(this, KontoActivity.class);
            //metoda pojedynczo
            //intent.putExtra("name", name);
            //intent.putExtra("name", surname);
            //metoda przekazania obiektu typu User
            User user = new User(nameEditText.getEditableText().toString(),
                    lastNameEditText.getEditableText().toString(), interestsSwitch.isChecked());
            intent.putExtra("user", user);
            startActivity(intent);
    }

    }
}
