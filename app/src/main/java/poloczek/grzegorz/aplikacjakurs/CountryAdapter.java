package poloczek.grzegorz.aplikacjakurs;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.ImageLoader;


import java.util.List;

/**
 * Created by Grzegorz on 2017-06-16.
 */

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.MyViewHolder> {

    private List<Country> countryList;
    private ImageLoader imageLoader;

    public CountryAdapter(List<Country> countryList) {
        this.countryList = countryList;
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.line_country,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Country countryObject = countryList.get(position);
        holder.countryTextView.setText(countryObject.getCountryName());
        holder.capitalTextView.setText(countryObject.getCountryCapital());
        imageLoader.displayImage(returnImageURI(countryObject.getFlag()), holder.flagImageView);
    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView countryTextView;
        private TextView capitalTextView;
        private ImageView flagImageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            countryTextView = (TextView) itemView.findViewById(R.id.countryLineNameText);
            capitalTextView = (TextView) itemView.findViewById(R.id.countryLineCapitalName);
            flagImageView = (ImageView) itemView.findViewById(R.id.countryLineImageFlag);
        }
    }

    public static String returnImageURI(int link){
        String imageUri = "drawable://" + link;
        return imageUri;
    }
}
