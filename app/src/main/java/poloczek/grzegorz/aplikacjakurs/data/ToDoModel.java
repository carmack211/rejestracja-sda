package poloczek.grzegorz.aplikacjakurs.data;

/**
 * Created by Grzegorz on 2017-06-16.
 */

public class ToDoModel {
    private int id;
    private String toDoTask;
    private String contentTask;

    public ToDoModel() {

    }
    public ToDoModel(int id, String toDoTask, String contentTask) {
        this.id = id;
        this.toDoTask = toDoTask;
        this.contentTask = contentTask;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToDoTask() {
        return toDoTask;
    }

    public void setToDoTask(String toDoTask) {
        this.toDoTask = toDoTask;
    }

    public String getContentTask() {return contentTask;}

    public void setContentTask(String contentTask) {this.contentTask = contentTask;}
}
