package poloczek.grzegorz.aplikacjakurs.data;

import android.provider.BaseColumns;

/**
 * Created by RENT on 2017-07-04.
 */

public final class MovieContract {

    private MovieContract() {

    }

    public static class MovieEntry implements BaseColumns {
        public static final String TABLE_NAME = "Movies";
        public static final String COLUMN_TITLE = "Title";
        public static final String COLUMN_BUDGET = "Budget";
        public static final String COLUMN_REALESE_DATE = "RealeseDate";
        public static final String COLUMN_MARK = "Mark";
    }
}
