package poloczek.grzegorz.aplikacjakurs.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by RENT on 2017-07-04.
 */

public class QuizDbQueries {

    private SQLiteDatabase database;
    private QuizDbHelper dbHelper;
    private List<QuizQuestionsModel> questionsList;



    public QuizDbQueries(Context context) {
        dbHelper = new QuizDbHelper(context);
    }

    public void save() throws SQLException {
        database =  dbHelper.getWritableDatabase();
    }

    public void load() throws SQLException {
        database = dbHelper.getReadableDatabase();
    }

    public void close() throws SQLException {
        database.close();
    }

    public void addCarToDataBase(QuizQuestionsModel quizQuestionsModel){
        load();
        ContentValues values = new ContentValues();
        values.put(QuizContract.QuizEntry.COLUMN_IMAGE, quizQuestionsModel.getImage());
        values.put(QuizContract.QuizEntry.COLUMN_ANSWERA, quizQuestionsModel.getAnswerA());
        values.put(QuizContract.QuizEntry.COLUMN_ANSWERB, quizQuestionsModel.getAnswerB());
        values.put(QuizContract.QuizEntry.COLUMN_ANSWERC, quizQuestionsModel.getAnswerC());
        values.put(QuizContract.QuizEntry.COLUMN_ANSWERD, quizQuestionsModel.getAnswerD());
        values.put(QuizContract.QuizEntry.COLUMN_CORRECT_ANSWER, quizQuestionsModel.getCorrectAnswer());
        database.insert(QuizContract.QuizEntry.TABLE_NAME, null, values);
        close();
    }

//    public List<QuizQuestionsModel> createRandomData(){
//        ArrayList<QuizQuestionsModel> quizList = new ArrayList<QuizQuestionsModel>();
//        QuizQuestionsModel quizQuestionsModel = new QuizQuestionsModel();
//        quizQuestionsModel.setImage("clio");
//        quizQuestionsModel.setAnswerA("stilo");
//        quizQuestionsModel.setAnswerB("clio");
//        quizQuestionsModel.setAnswerC("astra");
//        quizQuestionsModel.setAnswerD("twingo");
//        quizQuestionsModel.setCorrectAnswer("clio");
//        quizList.add(quizQuestionsModel);
//        addCarToDataBase(quizQuestionsModel);
//        return quizList;
//    }

    public List<QuizQuestionsModel>  initQuestions(){
        questionsList = returnQuizQuestions();
        if(questionsList.isEmpty()){
            addCarToDataBase(new QuizQuestionsModel("clio", "astra", "meriva", "jazz", "clio", "clio"));
            addCarToDataBase(new QuizQuestionsModel("mustang", "falcon", "mustang", "mondeo", "quattroporte", "mustang"));
            addCarToDataBase(new QuizQuestionsModel("stilo", "punto", "polo", "stilo", "golf", "stilo"));
            addCarToDataBase(new QuizQuestionsModel("golf", "golf", "focus", "stilo", "megane", "golf"));
            addCarToDataBase(new QuizQuestionsModel("m3", "m1", "m2", "m3", "m5", "m3"));
            addCarToDataBase(new QuizQuestionsModel("clk", "slk", "glk", "cls", "clk", "clk"));

            questionsList = returnQuizQuestions();
        }
        return questionsList;
    }

    public void addScoreToRankTable(String name, String score){
        save();
        ContentValues values = new ContentValues();

        values.put(QuizContract.RankingEntry.COLUMN_NAME, name);
        values.put(QuizContract.RankingEntry.COLUMN_SCORE, score);

       long f = database.insert(QuizContract.RankingEntry.TABLE_NAME, null, values);
        String senior = String.valueOf(f);
    }

    public List<QuizQuestionsModel> returnQuizQuestions(){
        load();
        Cursor cursor = database.query(QuizContract.
                                QuizEntry.
                                TABLE_NAME, null, null, null, null, null, null);
        List<QuizQuestionsModel> list = new ArrayList<QuizQuestionsModel>();

        QuizQuestionsModel quizQuestionsModel;

        if(cursor.getCount()>0) {
            for(int i=0; i<cursor.getCount(); i++){
                cursor.moveToNext();
                quizQuestionsModel = new QuizQuestionsModel();
                quizQuestionsModel.setImage(cursor.getString(1));
                quizQuestionsModel.setAnswerA(cursor.getString(2));
                quizQuestionsModel.setAnswerB(cursor.getString(3));
                quizQuestionsModel.setAnswerC(cursor.getString(4));
                quizQuestionsModel.setAnswerD(cursor.getString(5));
                quizQuestionsModel.setCorrectAnswer(cursor.getString(6));
                list.add(quizQuestionsModel);
            }
        }
        close();
        return list;

    }

    String sortOrder = QuizContract.RankingEntry.COLUMN_SCORE + " DESC";

    String[] projection = {QuizContract.RankingEntry._ID, QuizContract.RankingEntry.COLUMN_SCORE, QuizContract.RankingEntry.COLUMN_NAME};

    public List<QuizRankingModel> getRanking() {
        List<QuizRankingModel> listRanking = new ArrayList<>();
        load();
        Cursor c;
        try {
            c = database.query(QuizContract.RankingEntry.TABLE_NAME, projection, null, null, null, null, sortOrder);
            if (c == null) return null;
            c.moveToNext();
            do {
                int id = c.getInt(c.getColumnIndex(QuizContract.RankingEntry._ID));
                double score = c.getDouble(c.getColumnIndex(QuizContract.RankingEntry.COLUMN_SCORE));
                String name = c.getString(c.getColumnIndex(QuizContract.RankingEntry.COLUMN_NAME));

                QuizRankingModel ranking = new QuizRankingModel(id, score, name);
                listRanking.add(ranking);
            } while (c.moveToNext());
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listRanking;

    }

    public void deleteToDoTask(String id){
        String selection = QuizContract.RankingEntry._ID + " = ?";
        String selectionArguments[] = { id };
        // powyzsze rowna sie temu: np.  WHERE id = ? zamienia na WHERE id = 1;
        database.delete(QuizContract.RankingEntry.TABLE_NAME, selection, selectionArguments);
    }

}
