package poloczek.grzegorz.aplikacjakurs.data;

import android.provider.BaseColumns;

/**
 * Created by RENT on 2017-07-04.
 */

public final class ToDoContract {

    private ToDoContract() {

    }

    public static class ToDoEntry implements BaseColumns {
        public static final String TABLE_NAME = "Todo";
        public static final String COLUMN_TASK_NAME = "Task";
        public static final String COLUMN_CONTENT_NAME = "Content";
    }
}
