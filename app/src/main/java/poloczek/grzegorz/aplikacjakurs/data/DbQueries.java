package poloczek.grzegorz.aplikacjakurs.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-07-04.
 */

public class DbQueries {

    private SQLiteDatabase database;
    private DbHelper dbHelper;

    public DbQueries(Context context) {
        dbHelper = new DbHelper(context);
    }

    public void save() throws SQLException {
        database =  dbHelper.getWritableDatabase();
    }

    public void load() throws SQLException {
        database = dbHelper.getReadableDatabase();
    }

    public void close() throws SQLException {
        database.close();
    }

    public String addTaskToDataBase(String name, String content){

        ContentValues values = new ContentValues();

        values.put(ToDoContract.ToDoEntry.COLUMN_TASK_NAME, name);
        values.put(ToDoContract.ToDoEntry.COLUMN_CONTENT_NAME, content);

        long rowNumber = database.insert(ToDoContract.ToDoEntry.TABLE_NAME, null, values);

        return String.valueOf(rowNumber);
    }

    public String addFilmToDataBase(String name, int budget, String creationRelease, float mark){

        ContentValues values = new ContentValues();

        values.put(MovieContract.MovieEntry.COLUMN_TITLE, name);
        values.put(MovieContract.MovieEntry.COLUMN_BUDGET, budget);
        values.put(MovieContract.MovieEntry.COLUMN_REALESE_DATE, creationRelease);
        values.put(MovieContract.MovieEntry.COLUMN_MARK, mark);

        long rowNumber = database.insert(MovieContract.MovieEntry.TABLE_NAME, null, values);

        return String.valueOf(rowNumber);
    }


    public List<ToDoModel> returnToDoModel(){
        ArrayList<ToDoModel> toDoList = new ArrayList<ToDoModel>();

        Cursor cursor = database.query(ToDoContract.ToDoEntry.TABLE_NAME, null, null, null, null, null, null);

        ToDoModel toDoModel;

        if(cursor.getCount()>0) {
            for(int i=0; i<cursor.getCount(); i++){
                cursor.moveToNext();
                toDoModel = new ToDoModel();
                toDoModel.setId(cursor.getInt(0));
                toDoModel.setToDoTask(cursor.getString(1));
                toDoModel.setContentTask(cursor.getString(2));
                toDoList.add(toDoModel);
            }
        }

        return toDoList;
    }

    public List<MovieModel> returnMoviesFilm(){
        ArrayList<MovieModel> movieList = new ArrayList<MovieModel>();

        Cursor cursor = database.query(MovieContract.
                                MovieEntry.
                                TABLE_NAME, null, null, null, null, null, null);

        MovieModel movieModel;

        if(cursor.getCount()>0) {
            for(int i=0; i<cursor.getCount(); i++){
                cursor.moveToNext();
                movieModel = new MovieModel();
                movieModel.setId(cursor.getInt(0));
                movieModel.setTitle(cursor.getString(1));
                movieModel.setBudget(cursor.getInt(2));
                movieModel.setReleaseData(cursor.getString(3));
                movieModel.setMark(cursor.getFloat(4));
                movieList.add(movieModel);
            }
        }

        return movieList;
    }

    public void deleteRow(String id) {
        String seletion = MovieContract.MovieEntry._ID + " = ?";
        String selectionArguments[] = { id };
        database.delete(MovieContract.MovieEntry.TABLE_NAME, seletion, selectionArguments);
    }

    public void editRow(String id, int budget) {
        ContentValues values = new ContentValues();
        values.put(MovieContract.MovieEntry.COLUMN_BUDGET, budget);

        String selection = MovieContract.MovieEntry._ID + " = ?";
        String selectionArguments[] = { id };

        database.update(MovieContract.MovieEntry.TABLE_NAME, values, selection, selectionArguments);
    }

    public void clearDataBase(){
        database.execSQL("DELETE FROM " + ToDoContract.ToDoEntry.TABLE_NAME);
    }

    public void editToDoTask(String name, String id, String content){
        // ustalamy jaka kolumne chcemy zmienic i podajemy wartosc na jaka zmieniamy
        ContentValues values = new ContentValues();
        values.put(ToDoContract.ToDoEntry.COLUMN_TASK_NAME, name);
        values.put(ToDoContract.ToDoEntry.COLUMN_CONTENT_NAME, content);


        String selection = ToDoContract.ToDoEntry._ID + " = ?";
        String selectionArguments[] = { id };

        database.update(ToDoContract.ToDoEntry.TABLE_NAME, values, selection, selectionArguments);

    }


    public void deleteToDoTask(String id){
        String selection = ToDoContract.ToDoEntry._ID + " = ?";
        String selectionArguments[] = { id };
        // powyzsze rowna sie temu: np.  WHERE id = ? zamienia na WHERE id = 1;
        database.delete(ToDoContract.ToDoEntry.TABLE_NAME, selection, selectionArguments);

    }



}
