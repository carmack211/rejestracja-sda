package poloczek.grzegorz.aplikacjakurs.data;

/**
 * Created by RENT on 2017-07-05.
 */

public class MovieModel {

    private int id;
    private String title;
    private int budget;
    private String releaseData;
    private float mark;


    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getTitle() {return title;}

    public void setTitle(String title) {this.title = title;}

    public int getBudget() {return budget;}

    public void setBudget(int budget) {this.budget = budget;}

    public String getReleaseData() {return releaseData;}

    public void setReleaseData(String releaseData) {this.releaseData = releaseData;}

    public float getMark() {return mark;}

    public void setMark(float mark) {this.mark = mark;}

    public String returnRow(){
        return "Film: " + id + ", "+ title + ", " + budget + ", " + releaseData + ", " + mark;
    }

}
