package poloczek.grzegorz.aplikacjakurs.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by RENT on 2017-07-04.
 */

public class DbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERIOSN = 3;
    public static final String DATABASE_NAME = "Data.db";

    public static final String SQL_CREATE_TABLE_TO_DO = "CREATE TABLE " + ToDoContract.ToDoEntry.TABLE_NAME
            + "(" + ToDoContract.ToDoEntry._ID + " INTEGER PRIMARY KEY," +
            ToDoContract.ToDoEntry.COLUMN_TASK_NAME + " TEXT," + ToDoContract.ToDoEntry.COLUMN_CONTENT_NAME + " TEXT)";

    public static final String SQL_CREATE_TABLE = "CREATE TABLE " + MovieContract.MovieEntry.TABLE_NAME
            + "(" + MovieContract.MovieEntry._ID + " INTEGER PRIMARY KEY," +
            MovieContract.MovieEntry.COLUMN_TITLE + " TEXT," +
            MovieContract.MovieEntry.COLUMN_BUDGET + " INTEGER," +
            MovieContract.MovieEntry.COLUMN_REALESE_DATE + " TEXT, " +
            MovieContract.MovieEntry.COLUMN_MARK + " REAL)";

    private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXIST " + MovieContract.MovieEntry.TABLE_NAME;
    private static final String SQL_DELETE_TABLE_TODO = "DROP TABLE IF EXIST " + ToDoContract.ToDoEntry.TABLE_NAME;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERIOSN);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL(SQL_CREATE_TABLE);
        db.execSQL(SQL_CREATE_TABLE_TO_DO);
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE_TODO);
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }
}
