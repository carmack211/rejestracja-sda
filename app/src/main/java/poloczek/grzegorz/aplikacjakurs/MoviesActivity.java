package poloczek.grzegorz.aplikacjakurs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import poloczek.grzegorz.aplikacjakurs.data.DbQueries;
import poloczek.grzegorz.aplikacjakurs.data.MovieModel;

public class MoviesActivity extends AppCompatActivity {

    DbQueries dbQueries;

    EditText editTextName;

    EditText editTextBudget;

    EditText editTextReleaseDate;

    RecyclerView moviesRecyclerView;

    RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);
        ButterKnife.bind(this);
        dbQueries = new DbQueries(MoviesActivity.this);
        dbQueries.load();
        moviesRecyclerView = (RecyclerView) findViewById(R.id.movieRecyclerView);
        //textView = (TextView) findViewById(R.id.dataBaseTextView);
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextBudget = (EditText) findViewById(R.id.editTextBudget);
        editTextReleaseDate = (EditText) findViewById(R.id.editTextReleaseDate);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
    }

    public void addOnClick(View view) {
        dbQueries.addFilmToDataBase(editTextName.getEditableText().toString(),
                        Integer.parseInt(editTextBudget.getEditableText().toString()),
                        editTextReleaseDate.getEditableText().toString(), ratingBar.getRating());
    }

    public void showOnClick(View view) {
        showRecords();
    }

    private void showRecords() {

        List<MovieModel> list = dbQueries.returnMoviesFilm();

        LinearLayoutManager llm = new LinearLayoutManager(this);
        // ustawiamy Manager Layoutu
        moviesRecyclerView.setLayoutManager(llm);

        //utworzneie odstepu pomiedzy elementami listy
        DividerItemDecoration divider = new DividerItemDecoration(moviesRecyclerView.getContext(), llm.getOrientation());
        moviesRecyclerView.addItemDecoration(divider);

        // ustwaiamy nasz wlasny adapter do recyclerview
        MoviesAdapter movieAdapter = new MoviesAdapter(list, MoviesActivity.this);
        moviesRecyclerView.setAdapter(movieAdapter);
    }

    public void deleteOnClick(View view) {
        dbQueries.deleteRow("1");
        showRecords();

    }

    public void editOnClick(View view) {
        dbQueries.editRow("2", 100);
        showRecords();

    }

    @Override
    protected void onDestroy() {
        dbQueries.close();
        super.onDestroy();
    }

    public void clearOnClick(View view) {
        dbQueries.clearDataBase();
        showRecords();
    }
}
