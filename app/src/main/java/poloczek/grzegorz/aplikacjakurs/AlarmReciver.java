package poloczek.grzegorz.aplikacjakurs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.widget.Toast;

/**
 * Created by RENT on 2017-07-26.
 */

public class AlarmReciver extends BroadcastReceiver {
    Vibrator vibrator;
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "POBUDKA", Toast.LENGTH_LONG).show();
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        long[] params = {500,110,500,110,450,110,200,110,170,40,450,110,200,110,170,40,500};
        vibrator.vibrate(params, 3);
    }
}
