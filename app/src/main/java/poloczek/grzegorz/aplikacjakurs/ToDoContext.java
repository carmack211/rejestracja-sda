package poloczek.grzegorz.aplikacjakurs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ToDoContext extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_context);

        Intent i = getIntent();
        String content = i.getStringExtra("content");

        ToDoContentFragment toDoContentFragment = (ToDoContentFragment) getSupportFragmentManager().
                findFragmentById(R.id.toDoFragmentText);
        toDoContentFragment.passContent(content);

    }
}
