package poloczek.grzegorz.aplikacjakurs;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import poloczek.grzegorz.aplikacjakurs.data.DbQueries;
import poloczek.grzegorz.aplikacjakurs.data.ToDoModel;

public class ToDoActivity extends AppCompatActivity implements ViewChoice {

    DbQueries dbQueries;

    TextView textView;

    List<ToDoModel> toDoList;

    ToDoModel toDoModel;
    RecyclerView myRecycler;
    ToDoListAdapter toDoAdapter;
    String message;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do);

//        myRecycler = (RecyclerView) findViewById(R.id.dataBaseRecyclerView);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        myRecycler.setLayoutManager(linearLayoutManager);

        dbQueries = new DbQueries(ToDoActivity.this);
        dbQueries.load();

        toDoList = dbQueries.returnToDoModel();
        //toDoAdapter = new ToDoListAdapter(toDoList, getApplicationContext());

        //myRecycler.setAdapter(toDoAdapter);
    }

    public void addOnClick(View view) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ToDoActivity.this);
        alertDialog.setTitle("New Task");
        final EditText newText = new EditText(this);
        final EditText secondText = new EditText(this);
        LinearLayout layout = new LinearLayout(getApplicationContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        newText.setHint("Task Name");
        secondText.setHint("Task Description");
        layout.addView(newText);
        layout.addView(secondText);
        alertDialog.setView(layout);

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dbQueries.addTaskToDataBase(newText.getText().toString(), secondText.
                        getText().toString());
                ToDoListFragment fr = (ToDoListFragment) getSupportFragmentManager().
                        findFragmentById(R.id.fragmentList);
                fr.refreshAdapter();
//                toDoList = dbQueries.returnToDoModel();
//                toDoAdapter = new ToDoListAdapter(toDoList, getApplicationContext());
//                myRecycler.setAdapter(toDoAdapter);
                message = secondText.getText().toString();

            }
        });

        alertDialog.show();

//       dbQueries.
//               addTaskToDataBase(newText.getEditableText().toString(), secondText.getEditableText().toString());
//       toDoList = dbQueries.returnToDoModel();
//        ToDoListAdapter toDoListAdapter = new ToDoListAdapter(toDoList, getApplicationContext());
//       myRecycler.setAdapter(toDoListAdapter);

    }

    @Override
    protected void onDestroy() {
        dbQueries.close();
        super.onDestroy();
    }

    @Override
    public void onViewChoice(String text) {
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            Intent intent = new Intent(this, ToDoContext.class);
            intent.putExtra("content", text);
            startActivity(intent);
        } else {
            ToDoContentFragment fr = (ToDoContentFragment) getSupportFragmentManager().findFragmentById(R.id.toDoFragmentText);
            fr.passContent(text);
        }
    }
}
