package poloczek.grzegorz.aplikacjakurs;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {


    public SecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View ciew = inflater.inflate(R.layout.fragment_second, container, false);
        ImageView zdj = (ImageView) ciew.findViewById(R.id.fragmentSecondImageView);
        ImageLoader imageLoader = ImageLoader.getInstance();
        String url = "http://android.com.pl/images/user-images/lwr/2016/09/AndroidPIT-Android-Nougat-9734-w782.jpg";
        imageLoader.displayImage(url, zdj);
        return ciew;
    }

}
