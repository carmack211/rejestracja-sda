package poloczek.grzegorz.aplikacjakurs;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import poloczek.grzegorz.secretphoto.PhotoSecretReturner;
import uk.co.senab.photoview.PhotoViewAttacher;

public class CzwarteActivity extends AppCompatActivity {

    @BindView(R.id.imageView) ImageView kontrolkaZdjecia;
    @BindView(R.id.image) ImageView palettePhoto;
    PhotoViewAttacher zoomer;

    private TextView vibrantTextView;
    private TextView lightVibrantTextView;
    private TextView darkVibrantTextView;
    private TextView mutedTextView;
    private TextView lightMutedTextView;
    private TextView darkMutedTextView;

    private ListView listView;
    private SwatchAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_czwarte);

        ButterKnife.bind(this);

        adapter = new SwatchAdapter( this );

        initViews();

        palettePhoto.setImageResource(loadPalettePhoto());

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), loadPalettePhoto());
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                setViewSwatch(vibrantTextView, palette.getVibrantSwatch());
                setViewSwatch( lightVibrantTextView, palette.getLightVibrantSwatch() );
                setViewSwatch( darkVibrantTextView, palette.getDarkVibrantSwatch() );
                setViewSwatch( mutedTextView, palette.getMutedSwatch() );
                setViewSwatch( lightMutedTextView, palette.getLightMutedSwatch() );
                setViewSwatch( darkMutedTextView, palette.getDarkMutedSwatch() );

                for( Palette.Swatch swatch : palette.getSwatches() ) {
                    adapter.add(swatch);
                }

                adapter.sortSwatches();
                adapter.notifyDataSetChanged();
            }
        });

    }

    public void loadPalettePhoto(View view) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        PhotoSecretReturner zwrocRandomoweZdjecie = new PhotoSecretReturner();
        String foto = zwrocRandomoweZdjecie.zwrocZdjecieLista();
        imageLoader.displayImage(foto, kontrolkaZdjecia);
        //zoomer = new PhotoViewAttacher(kontrolkaZdjecia);
        //zoomer.update();

    }

    public void setViewSwatch(TextView textView, Palette.Swatch swatch){
        if(swatch != null){
            textView.setTextColor(swatch.getTitleTextColor());
            textView.setBackgroundColor(swatch.getRgb());
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    private void initViews() {
        listView = (ListView) findViewById( R.id.list );
        vibrantTextView = (TextView) findViewById( R.id.vibrant );
        lightVibrantTextView = (TextView) findViewById( R.id.light_vibrant );
        darkVibrantTextView = (TextView) findViewById( R.id.dark_vibrant );
        mutedTextView = (TextView) findViewById( R.id.muted );
        lightMutedTextView = (TextView) findViewById( R.id.light_muted );
        darkMutedTextView = (TextView) findViewById( R.id.dark_muted );
    }

    public int loadPalettePhoto(){
        Integer[] images = {R.drawable.austria, R.drawable.poland,
                R.drawable.belgium, R.drawable.italy};
        Random random = new Random();
        return images[random.nextInt(images.length)];
    }
}
