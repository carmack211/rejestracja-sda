package poloczek.grzegorz.aplikacjakurs;


import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import poloczek.grzegorz.aplikacjakurs.data.DbQueries;
import poloczek.grzegorz.aplikacjakurs.data.QuizDbQueries;


public class QuizFinish extends AppCompatActivity {
    private int score;
    private int totalQuestion;
    private int recivedScore;
    private int recivedQuestions;
    private TextView scorePoints;
    private MediaPlayer mediaPlayer;
    private QuizDbQueries dbQueries;
    private int countSave = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_finish);

        dbQueries = new QuizDbQueries(QuizFinish.this);
        dbQueries.load();

        scorePoints = (TextView) findViewById(R.id.quizUserScore);

        recivedScore = getIntent().getExtras().getInt("SCORE",score);
        recivedQuestions = getIntent().getExtras().getInt("TOTAL", totalQuestion);
        scorePoints.setText(String.format("%d/%d", recivedScore, recivedQuestions));

        playSound();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.finish();
    }

    public void playSound(){
        float value = (float)recivedScore / (float)recivedQuestions;
        if (value > 0.75f){
            mediaPlayer = MediaPlayer.create(this, R.raw.engine);
            mediaPlayer.start();
        } else if (value >= 0.3f){
            mediaPlayer = MediaPlayer.create(this, R.raw.engine_start);
            mediaPlayer.start();
        } else {
            mediaPlayer = MediaPlayer.create(this, R.raw.fail);
            mediaPlayer.start();
        }
    }

    public void saveScore(View view) {
        if (countSave == 0) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(QuizFinish.this);
            alertDialog.setTitle(R.string.quiz_save_score_alert);
            final EditText newText = new EditText(this);
            LinearLayout layout = new LinearLayout(getApplicationContext());
            layout.setOrientation(LinearLayout.VERTICAL);
            newText.setHint(R.string.quiz_scoreboard_nickname);
            layout.addView(newText);
            alertDialog.setView(layout);

            alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertDialog.setPositiveButton(R.string.quiz_save_score_alert_positive, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dbQueries.addScoreToRankTable(newText.getText().toString(), String.valueOf(recivedScore));
                }
            });
            countSave ++;
            alertDialog.show();
        } else {
            Toast.makeText(this, R.string.quiz_save_score_already, Toast.LENGTH_LONG).show();
        }
    }

    public void showScoreboard(View view) {
        Intent intent = new Intent(this, QuizScoreboardActivity.class);
        startActivity(intent);
    }
}
