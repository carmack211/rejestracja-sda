package poloczek.grzegorz.aplikacjakurs;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LoginActivity extends AppCompatActivity {
    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
    private Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    private Matcher matcher;
    private MediaPlayer mediaPlayer;
    private EditText loginEditText;
    private EditText passwordEditText;


    private BroadcastReceiver broadcastReceiver;
    private BroadcastReceiver brodcastReciverChargingOff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        hideKeyboard();
        loginEditText = (EditText) findViewById(R.id.email);
        passwordEditText = (EditText) findViewById(R.id.password);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(LoginActivity.this, "Ładowanie baterii", Toast.LENGTH_SHORT).show();
            }
        };

        brodcastReciverChargingOff = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(LoginActivity.this, "Bateria nie ładuję się", Toast.LENGTH_SHORT).show();
            }
        };

        SharedPreferences sharedPreferences = getSharedPreferences("logins", Context.MODE_PRIVATE);
        String login = sharedPreferences.getString(getString(R.string.PREF_LOGIN),"");
        String password = sharedPreferences.getString(getString(R.string.PREF_PASSWORD),"");
        loginEditText.setText(login);
        passwordEditText.setText(password);

    }

    @Override
    protected void onResume(){
        super.onResume();

        //TODO
        //wykonać metodę do obsługi brodcastReciver
        //registerReceiver(broadcastReceiver, new IntentFilter(Intent.ACTION_POWER_CONNECTED));
        //registerReceiver(brodcastReciverChargingOff, new IntentFilter(Intent.ACTION_POWER_DISCONNECTED));
    }

    @Override
    protected void onPause(){
        super.onPause();
        //unregisterReceiver(broadcastReceiver);
    }


    public void singIn(View view) {
        playBeep();
        final TextInputLayout usernameWrapper = (TextInputLayout) findViewById(R.id.inputLayout1);
        final TextInputLayout passwordWrapper = (TextInputLayout) findViewById(R.id.inputLayout2);
        String username = usernameWrapper.getEditText().getText().toString();
        String password = passwordWrapper.getEditText().getText().toString();
        if (!validateEmail(username)) {
            usernameWrapper.setError("Wprowadź poprawny adres email!");
        } else{
            usernameWrapper.setErrorEnabled(false);
        }
        if (!validatePassword(password)) {
            passwordWrapper.setError("Wprowadzone hasło jest za krótkie!");
            } else {
                passwordWrapper.setErrorEnabled(false);
            }
    }

    public boolean validatePassword(String password) {
        return password.length() > 9;
    }

    public boolean validateEmail(String email) {
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void register(View view) {
        playBeep();
        Intent intent = new Intent(LoginActivity.this, RejestracjaActivity.class);
        startActivity(intent);
    }

    public void playBeep(){
        mediaPlayer = MediaPlayer.create(this, R.raw.beep);
        mediaPlayer.start();
    }
}

