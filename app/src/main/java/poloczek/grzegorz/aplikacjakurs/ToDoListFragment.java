package poloczek.grzegorz.aplikacjakurs;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import poloczek.grzegorz.aplikacjakurs.data.DbQueries;
import poloczek.grzegorz.aplikacjakurs.data.ToDoModel;

public class ToDoListFragment extends Fragment {

    private RecyclerView myRecyclerView;
    private List<ToDoModel> taskList;
    ToDoListAdapter toDoListAdapter;
    DbQueries connectionToDb;

    public ToDoListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_to_do_list, container, false);
        myRecyclerView = (RecyclerView) v.findViewById(R.id.myRecyclerViewListFragment);
        return v;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        connectionToDb = new DbQueries(getActivity());
        connectionToDb.load();

        taskList = connectionToDb.returnToDoModel();

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        myRecyclerView.setLayoutManager(lm);

        DividerItemDecoration divider = new DividerItemDecoration
                (myRecyclerView.getContext(), lm.getOrientation());

        myRecyclerView.addItemDecoration(divider);

        toDoListAdapter = new ToDoListAdapter(taskList, getActivity());
        myRecyclerView.setAdapter(toDoListAdapter);
    }

    public void refreshAdapter(){
        taskList = connectionToDb.returnToDoModel();
        toDoListAdapter = new ToDoListAdapter(taskList, getActivity());
        myRecyclerView.setAdapter(toDoListAdapter);
    }
}
