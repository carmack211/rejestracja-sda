package poloczek.grzegorz.aplikacjakurs;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewSecondFragment extends Fragment {

    private String message;
    private MyActivityListener activityListener;


    public interface MyActivityListener{
        public void sendMessage(String msg);
    }

    public NewSecondFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_new_second, container, false);
        Button button = (Button) v.findViewById(R.id.fragmentFirstButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });

        Button button2 = (Button) v.findViewById(R.id.fragmentSecondButton);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityListener.sendMessage("TAJNY KOD: 8080");
            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null) {
            message = bundle.getString("CLASSIFIED");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activityListener = (MyActivityListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityListener = null;
    }
}
