package poloczek.grzegorz.aplikacjakurs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MainMenuActivity extends AppCompatActivity {
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        //imageloaderconfiguration WYKORZYSTYWANY JEST TYLKO RAZ !!
        // do obsługi zdjęć, powinien być wpisany w głównym activity aplikacji !
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.
                Builder(getApplicationContext()).
                build();
        ImageLoader.getInstance().init(configuration);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainMenuActivity.this, ServiceActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void firstExc(View view) {
        Intent intent = new Intent(MainMenuActivity.this, MainActivity.class);
        startActivity(intent);
    }

    public void secondExc(View view) {
        Intent intent = new Intent(MainMenuActivity.this, SMSActivity.class);
        startActivity(intent);
    }

    public void thirdExc(View view) {
        Intent intent = new Intent(MainMenuActivity.this, CzwarteActivity.class);
        startActivity(intent);
    }

    public void fourthExc(View view) {
        Intent intent = new Intent(MainMenuActivity.this, AccelerometrActivity.class);
        startActivity(intent);
    }

    public void fifthExc(View view) {
        Intent intent = new Intent(MainMenuActivity.this, ZasobyActivity.class);
        startActivity(intent);
    }


    public void sixthExc(View view) {
        Intent intent = new Intent(MainMenuActivity.this, LoginActivity.class);
        startActivity(intent);
    }


    public void seventhExc(View view) {
        Intent intent = new Intent(MainMenuActivity.this, KalkulatorActivity.class);
        startActivity(intent);
    }

    public void eighthExc(View view) {
        Intent intent = new Intent(MainMenuActivity.this, PanstwaActivity.class);
        startActivity(intent);
    }

    public void ninthExc(View view) {
        Intent intent = new Intent(MainMenuActivity.this, ServiceActivity.class);
        startActivity(intent);
    }

    public void tenthExc(View view) {
        Intent intent = new Intent(MainMenuActivity.this, TabActivity.class);
        startActivity(intent);
    }

    public void eleventh(View view) {
        Intent intent = new Intent(MainMenuActivity.this, ToDoActivity.class);
        startActivity(intent);
    }

    public void twelfth(View view) {
        Intent intent = new Intent(MainMenuActivity.this, RestaurantActivity.class);
        startActivity(intent);
    }

    public void thirteenth(View view) {
        Intent intent = new Intent(MainMenuActivity.this, MoviesActivity.class);
        startActivity(intent);
    }

    public void fourteenth(View view) {
        Intent intent = new Intent(MainMenuActivity.this, FragmentLessonActivity.class);
        startActivity(intent);
    }

    public void fifteenth(View view) {
        Intent intent = new Intent(MainMenuActivity.this, ImageActivity.class);
        startActivity(intent);
    }

    public void sixteenth(View view) {
        Intent intent = new Intent(MainMenuActivity.this, AsyncTaskActivity.class);
        startActivity(intent);
    }

    public void quizStart(View view) {
        Intent intent = new Intent(MainMenuActivity.this, QuizStartActivity.class);
        startActivity(intent);
    }
}
