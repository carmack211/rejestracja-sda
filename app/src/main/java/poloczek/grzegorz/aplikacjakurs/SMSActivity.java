package poloczek.grzegorz.aplikacjakurs;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import org.greenrobot.greendao.database.Database;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import poloczek.grzegorz.aplikacjakurs.db.DaoMaster;
import poloczek.grzegorz.aplikacjakurs.db.DaoSession;
import poloczek.grzegorz.aplikacjakurs.db.Product;
import poloczek.grzegorz.aplikacjakurs.db.ProductDao;

public class SMSActivity extends AppCompatActivity {

    EditText numerTelefonu;
    EditText tekstWiadomosci;
    TextView smsText;
    Integer valid_len;
    Button btnSMS;
    FloatingActionButton floatingBtnSMS;
    Button btnDostep;
    FloatingActionButton floatingAccessSMS;
    String restaurantCart = "";
    String filename = "mojPlik";
    FileInputStream fileInputStream;
    TextView lineTelephoneNumber;
    ListView smsListView;

    // stala dzieki ktorej po odpowiedzi od aplikacji o statusie przyznania dostepu bedziemy
    // wiedziec ze jest to odpowiedz na dane nasze wywolanie
    final int MY_PERMISSIONS_REQUEST_WRITE_SMS = 1;
    final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 2;
    final int MY_PERMISSIONS_REQUEST_READ_LOGS = 3;

    // nasz TAG za pomoca ktorego bedziemy mogli filtrowac wiadomosci w Android Monitor
    public static final String TAG = "GrzegorzApp";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sms_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_call) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                        Manifest.permission.CALL_PHONE)){
                    showExplanation("Potrzebujemy pozwolenia", "Próba nawiązania połączenia",
                            Manifest.permission.CALL_PHONE, MY_PERMISSIONS_REQUEST_CALL_PHONE);
                }else{
                    requestPermissions(Manifest.permission.CALL_PHONE, MY_PERMISSIONS_REQUEST_CALL_PHONE);
                }
            }else{
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + (numerTelefonu.getEditableText().toString())));
                startActivity(intent);
            }
        }

        if (id == R.id.action_restaurant) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                        Manifest.permission.SEND_SMS)){
                    showExplanation("Potrzebujemy pozwolenia", "Próba wysłania menu restauracji",
                            Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_WRITE_SMS);
                }else{
                    requestPermissions(Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_WRITE_SMS);
                }
            }else{
                SmsManager smsManager = SmsManager.getDefault();
                String phoneNo = numerTelefonu.getText().toString();
                DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "users-db");
                Database database = helper.getWritableDb();
                DaoSession daoSession = new DaoMaster(database).newSession();
                ProductDao productDao = daoSession.getProductDao();
                List<Product> list = productDao.queryBuilder().list();

                for(int i=0; i<list.size(); i++){
                    Product value = list.get(i);
                    restaurantCart += value.getName().toString() + "\n";
                }
                smsManager.sendTextMessage(phoneNo, null, restaurantCart, null, null);
            }
        }

        if (id == R.id.action_save_data) {

            String toSave = numerTelefonu.getText().toString();
            FileOutputStream fileOutputStream;

            try {
                fileOutputStream = openFileOutput(filename, Context.MODE_PRIVATE);
                fileOutputStream.write(toSave.getBytes());
                fileOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Snackbar.make(findViewById(R.id.smsLayout), "Zapisano", Snackbar.LENGTH_SHORT).show();
        }

        if (id == R.id.action_load_data) {
            try {
                fileInputStream = openFileInput(filename);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                numerTelefonu.setText(stringBuilder.toString());
            }catch (Exception e){
                e.printStackTrace();
            }
            Snackbar.make(findViewById(R.id.smsLayout), "Odczytane", Snackbar.LENGTH_SHORT).show();
        }

        if (id == R.id.action_show_history) {

            List<String> adresaci = new ArrayList<>();
            Set<String> set = new HashSet<>();
            Uri smsyInbox = Uri.parse("content://sms/inbox");

            Cursor cursor = getContentResolver().query(smsyInbox, null, null, null, null);

            int indexAdres = cursor.getColumnIndex("address");

            while (cursor.moveToNext()) {
                // pobieram kolumne o indexie 2 w ktorej sa informacje o adresacie
                String adresat = cursor.getString(indexAdres);
                set.add(adresat);
            }
            adresaci.addAll(set);
            smsListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, adresaci));

        }
            return super.onOptionsItemSelected(item);
        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

        smsListView = (ListView) findViewById(R.id.smsRecyclerView);

        smsListView = (ListView) findViewById(R.id.smsRecyclerView);
        smsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // odczytuje klikniety numer telefonu
                String numerTel= (String) parent.getItemAtPosition(position);
                // wywoluje funkcje ktora zwraca liste smsow dla tego unikalnego numeru
                List<String> listaSmsow = przeczytajSmsDlaNumer(numerTel);

                // laduje adapter
                smsListView.setAdapter(new ArrayAdapter<String>(SMSActivity.this, android.R.layout.simple_list_item_1, listaSmsow));

                // ustawiam numer telefonu wybrany
                numerTelefonu.setText(numerTel);
            }
        });

        numerTelefonu = (EditText) findViewById(R.id.numerTelefonu);
        tekstWiadomosci = (EditText) findViewById(R.id.textWiadomosci);

        smsText = (TextView) findViewById(R.id.smsText);

        lineTelephoneNumber = (TextView) findViewById(R.id.lineTelephoneNumber);

        btnSMS = (Button) findViewById(R.id.btnSMS);
        floatingBtnSMS = (FloatingActionButton) findViewById(R.id.floatingBtnSMS);
        btnDostep = (Button) findViewById(R.id.btnDostep);
        floatingAccessSMS = (FloatingActionButton) findViewById(R.id.floatingAccessSMS);

        floatingBtnSMS.setEnabled(false);

        btnSMS.setOnClickListener(onSMSClick);
        floatingBtnSMS.setOnClickListener(onSMSClick);
        btnDostep.setOnClickListener(onDostepClick);
        floatingAccessSMS.setOnClickListener(onDostepClick);

        numerTelefonu.setFilters(new InputFilter[] {new InputFilter.LengthFilter(9) });

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_mail_outline_white_24dp);

        Intent intent = getIntent();
        String totalPrice = intent.getStringExtra("totalPrice");
        tekstWiadomosci.setText("Całkowity koszt wynosi: " + totalPrice + " zł");


        tekstWiadomosci.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Check_SMS_Length(tekstWiadomosci); // pass your EditText Obj here.
            }
        });

        checkAccess();

    }

    private List<String> przeczytajSmsDlaNumer(String numerTel) {
        List<String> smsy = new ArrayList<>();
        Uri smsyInbox = Uri.parse("content://sms/inbox");

        String[] kolumnyKtoreChcemyWyswietlic = new String[] {"body"};
        String warunekWhere = "address='"+ numerTel + "'";
        Cursor cursor = getContentResolver().query(smsyInbox, kolumnyKtoreChcemyWyswietlic, warunekWhere, null, null);
        // select KolumnaBody From Smsy/Inbox Where Address = podanyNumerTelefonu

        // znajdujemy indexKolumny
        int indexBody = cursor.getColumnIndex("body");

        while(cursor.moveToNext()){
            // pobieram kolumne o indexie 2 w ktorej sa informacje o adresacie
            String trescSms = cursor.getString(indexBody);
            smsy.add(trescSms);

        }
        cursor.close();

        return smsy;
    }

    private void checkAccess() {
        if(android.support.v4.app.ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED){

            // za pomoca tej funkcji sprawdzamy czy uzytkownik po raz pierwszy juz blokowal dostep do sms
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CALL_LOG)){
                // jesli dostep blokowal pokazujemy po co nam to potrzebne
                showExplanation("Potrzebujemy pozwolenia", "Chcemy odczytać połaczenia w telefonie",
                        Manifest.permission.READ_CALL_LOG, MY_PERMISSIONS_REQUEST_READ_LOGS);
            }else{
                // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
                requestPermissions(Manifest.permission.READ_CALL_LOG, MY_PERMISSIONS_REQUEST_READ_LOGS);
            }
        }else{
            // z Edittext za pomoca funkcji getText().toString() wykonanej na kontrolce pobieramy wpisany tekst
            // sendsms to wlasna funkcja do wysylania smsow opisana ponizej
            //lineTelephoneNumber.setText(getCallDetails());
        }
    }

    // listener ten to definicja reakcji na zdarenie w tym wypadku nacisniecie klikanego elementu
    public View.OnClickListener onSMSClick = new View.OnClickListener(){
        public void onClick(View v){
              // z Edittext za pomoca funkcji getText().toString() wykonanej na kontrolce pobieramy wpisany tekst
             // sendsms to wlasna funkcja do wysylania smsow opisana ponizej
                sendSms(numerTelefonu.getText().toString(), tekstWiadomosci.getText().toString());

        }
    };

    // wlasna funkcja ktora przyjmuje numer telefonu i tresc wiadomosci
    private void sendSms(String phoneNo, String msg) {
        try {
            //wykorzystujemy smsmanager czyli wbudowane api do zarzadzania smsami
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);

            if(tekstWiadomosci.getText().length() != 0) {
                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.smsLayout), "Wysłano wiadomość SMS", Snackbar.LENGTH_SHORT);
                mySnackbar.show();
            }
            // za pomoca Log mozemy zrobic log momencie wyslania SMS . Ten log jest widoczny w ANdroid Monitorze.
            Log.d(TAG, "SMS Wysłany");

            // ponizej na dwa sposoby czyscimy wpisane kontrolki
            numerTelefonu.setText("");
            tekstWiadomosci.getText().clear();

            setResult(RESULT_OK);
            finish();

        } catch (Exception ex) {
            if (numerTelefonu.getText().length() == 0) {
                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.smsLayout), "WPROWADŹ NUMER TELEFONU", Snackbar.LENGTH_LONG);

                // sposób costumizacji snackbara
//                View sbView = mySnackbar.getView();
//                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//                textView.setTextColor(this.getResources().getColor(R.color.red));
//                sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.primary_light));

                mySnackbar.show();
            } else if (tekstWiadomosci.getText().length() == 0){
                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.smsLayout), "WPROWADŹ TREŚĆ WIADOMOŚCI", Snackbar.LENGTH_LONG);
                mySnackbar.show();
            }
            Log.d(TAG, "SMS Nie wysłany");
            ex.printStackTrace();

        }
    }

    public View.OnClickListener onDostepClick = new View.OnClickListener(){
        public void onClick(View v){
            // sprawdzamy czy jest przyznay dostep
            if(android.support.v4.app.ActivityCompat.checkSelfPermission(SMSActivity.this, Manifest.permission.SEND_SMS)
                    != PackageManager.PERMISSION_GRANTED){

                // za pomoca tej funkcji sprawdzamy czy uzytkownik po raz pierwszy juz blokowal dostep do sms
                if (ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                        Manifest.permission.SEND_SMS)){
                    // jesli dostep blokowal pokazujemy po co nam to potrzebne
                    showExplanation("Potrzebujemy pozwolenia", "Chcemy wysłać SMS który napisałeś, więc potrzebujemy pozwolenia",
                            Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_WRITE_SMS);
                }else{
                    // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
                    requestPermissions(Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_WRITE_SMS);
                }
            }else{
                // wlaczamy przycisk sms jesli dostep przyznay ( mozna to sprawdzac z automatu w oncreate aby za kazdym razem nie sprawdzac tutaj)
                btnSMS.setEnabled(true);
                floatingBtnSMS.setEnabled(true);
            }
        }
    };

    // funkcja ktora pokazuje okienko systemowe z prosba o dany kod
    private void requestPermissions(String permissionName, int permissionRequestCode){
       ActivityCompat.requestPermissions(this, new String[]{permissionName} , permissionRequestCode);
    }

    // wlasna funkcja ktora pokazuje okienko z wyjasnieniem prosby o dostep
    private void showExplanation(String title, String message, final String permission, final int permissionRequestCode){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                requestPermissions(permission, permissionRequestCode);
            }
        });
        builder.show();
    }

    // metoda wywolywana za kazdym razem gdy uzytkownik podejmie decyzje o dostepie
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_SMS: {
                // jesli uzytkownik dal anuluj to dlugosc listy bedzie pusta
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    btnSMS.setEnabled(true);
                    floatingBtnSMS.setEnabled(true);
                    // dostep przyznany - mozemy zrobic co chcemy
                    Log.d(TAG, "Dostęp przyznany");
                } else {
                    Log.d(TAG, "Dostęp nie przyznany");
                    //  dostep nie przyznany ! musimy obsluzyc ten problem w aplikacji
                    // ponizej dodatkowo sprawdzamy czy zaznaczyl never ask again
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        // user rejected the permission
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                                Manifest.permission.SEND_SMS);
                        if (!showRationale) {
                            Log.d(TAG, "Uzytkownik zaznaczyl never ask again");
                        }
                    }
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // jesli uzytkownik dal anuluj to dlugosc listy bedzie pusta
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // dostep przyznany - mozemy zrobic co chcemy
                    Log.d(TAG, "Dostęp przyznany");
                } else {
                    Log.d(TAG, "Dostęp nie przyznany");
                    //  dostep nie przyznany ! musimy obsluzyc ten problem w aplikacji
                    // ponizej dodatkowo sprawdzamy czy zaznaczyl never ask again
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        // user rejected the permission
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                                Manifest.permission.CALL_PHONE);
                        if (!showRationale) {
                            Log.d(TAG, "Uzytkownik zaznaczyl never ask again");
                        }
                    }
                }
                return;
            }

        }
    }

    public void exitActivity(View view) {
        Intent i = new Intent(SMSActivity.this, MainMenuActivity.class);
        startActivity(i);
    }

    public void Check_SMS_Length(EditText edt) throws NumberFormatException {
        try {
            if (edt.getText().toString().length() <= 0) {
                edt.setError("Sorry...!! Its Mandatory Field");
                valid_len = 0;
                smsText.setText("0/164");

            } else {
                valid_len = edt.getText().toString().length();
                smsText.setText(String.valueOf(valid_len) + "/" + 164);
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
    }
}
