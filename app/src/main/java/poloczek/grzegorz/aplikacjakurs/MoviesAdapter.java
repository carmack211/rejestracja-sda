package poloczek.grzegorz.aplikacjakurs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import poloczek.grzegorz.aplikacjakurs.data.MovieModel;

/**
 * Created by Grzegorz on 2017-07-19.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

    private List<MovieModel> movieList;
    Context context;

    public MoviesAdapter(List<MovieModel> movieList, Context context){
        this.movieList = movieList;
        this.context = context;
    }

    @Override
    public MoviesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_movies, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MoviesAdapter.MyViewHolder holder, final int position) {

        final MovieModel item = movieList.get(position);

        holder.movieName.setText(item.getTitle());
        holder.movieProduction.setText(item.getReleaseData());
        holder.movieMark.setRating(item.getMark());

    }



    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView movieName;
        TextView movieProduction;
        RatingBar movieMark;

        public MyViewHolder(View itemView){
            super(itemView);

            movieName = (TextView) itemView.findViewById(R.id.txtTytul);
            movieProduction = (TextView) itemView.findViewById(R.id.txtRokProdukcji);
            movieMark = (RatingBar) itemView.findViewById(R.id.ratingBarFilm);


        }
    }
}
