package poloczek.grzegorz.aplikacjakurs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.greendao.database.Database;

import java.util.List;

import poloczek.grzegorz.aplikacjakurs.data.DbQueries;
import poloczek.grzegorz.aplikacjakurs.data.ToDoModel;
import poloczek.grzegorz.aplikacjakurs.db.Basket;
import poloczek.grzegorz.aplikacjakurs.db.BasketDao;
import poloczek.grzegorz.aplikacjakurs.db.DaoMaster;
import poloczek.grzegorz.aplikacjakurs.db.DaoSession;
import poloczek.grzegorz.aplikacjakurs.db.Product;
import poloczek.grzegorz.aplikacjakurs.db.ProductDao;

/**
 * Created by Grzegorz on 2017-06-16.
 */

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.MyViewHolder> {

    private List<Product> productList;
    private Context context;
    private ProductDao productDao;
    private DaoSession daoSession;
    private RestaurantAdapter restaurantAdapter;

    private Product product;
    private RecyclerView myRecyclerView;

    //konstruktor przyjmuje contex, zeby wiedzial w jakim operujemy activity
    //oraz liste w tym przypadku produktow, jako lista danych
    public RestaurantAdapter(List<Product> productList, Context context) {

        this.productList = productList;
        this.context = context;

    }

    //ladowany jest wyglad xml, ktory jest odpowiedzialny za wyglad wierszy
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.line_restaurant,parent,false);
        return new MyViewHolder(view);
    }

    //laczymy dane z kontrolkami z holdera
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        //bierze obiekt z listy np. 1 i tworzy odpowiednio nazwe produktu i cene, na liscie itd.
        final Product item = productList.get(position);
         holder.productName.setText(item.getName());
         holder.productPrice.setText(String.valueOf(item.getPrice()));
         holder.addProduct.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "users-db");
                Database database = helper.getWritableDb();
                DaoSession daoSession = new DaoMaster(database).newSession();
                BasketDao basketDao = daoSession.getBasketDao();

                List<Basket> basketList = basketDao.
                        queryBuilder().where(BasketDao.Properties.ProductId.eq(item.getId())).list();

                if(basketList.size() == 0) {

                    Basket basket = new Basket();
                    basket.setQuantity(1);
                    basket.setProductId(item.getId());

                    basketDao.insert(basket);
                }else {
                    Basket basketUpdate = basketList.get(0);
                    basketUpdate.setQuantity(basketUpdate.getQuantity()+1);
                    basketDao.update(basketUpdate);
                }
            }
        });
        holder.editProduct.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Edit product");

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);

                LinearLayout.LayoutParams doTextu = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);

                LinearLayout linear = new LinearLayout(context);
                linear.setOrientation(LinearLayout.VERTICAL);
                linear.setLayoutParams(lp);

                final EditText productName = new EditText(context);
                productName.setHint("Product name");
                productName.setLayoutParams(doTextu);
                linear.addView(productName);

                final EditText price = new EditText(context);
                price.setHint("Price");
                price.setInputType(InputType.TYPE_CLASS_NUMBER);
                price.setLayoutParams(doTextu);
                linear.addView(price);

                alertDialog.setView(linear);

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "users-db");
                        Database database = helper.getWritableDb();
                        daoSession = new DaoMaster(database).newSession();
                        productDao = daoSession.getProductDao();
                        item.setName(productName.getText().toString());
                        item.setPrice(Float.valueOf(price.getText().toString()));
                        productDao.update(item);
                        readData();
                        holder.productName.setText(item.getName());
                        holder.productPrice.setText(String.valueOf(item.getPrice()));

                    }
                });

                alertDialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView productName;
        private TextView productPrice;
        private ImageButton addProduct;
        private ImageButton editProduct;
        //MyVievHolder, okresla na jakich kontrolkach chcemy operowac

        public MyViewHolder(View itemView) {
            super(itemView);
            productName = (TextView) itemView.findViewById(R.id.txtProduct);
            productPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            addProduct = (ImageButton) itemView.findViewById(R.id.addProduct);
            editProduct = (ImageButton) itemView.findViewById(R.id.editProduct);
        }
    }

    public void readData(){
        productList = productDao.queryBuilder().list();
    }

}
