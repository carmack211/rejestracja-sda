package poloczek.grzegorz.aplikacjakurs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.greenrobot.greendao.database.Database;

import java.util.List;

import poloczek.grzegorz.aplikacjakurs.db.Basket;
import poloczek.grzegorz.aplikacjakurs.db.BasketDao;
import poloczek.grzegorz.aplikacjakurs.db.DaoMaster;
import poloczek.grzegorz.aplikacjakurs.db.DaoSession;
import poloczek.grzegorz.aplikacjakurs.db.Product;
import poloczek.grzegorz.aplikacjakurs.db.ProductDao;

/**
 * Created by Grzegorz on 2017-06-16.
 */

public class BasketAdapter extends RecyclerView.Adapter<BasketAdapter.MyViewHolder> {

    private List<Basket> basketList;
    private Context context;

    public BasketAdapter(List<Basket> basketList, Context context) {

        this.basketList = basketList;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.line_basket,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Basket item = basketList.get(position);

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "users-db");
        Database database = helper.getWritableDb();
        DaoSession daoSession = new DaoMaster(database).newSession();
        ProductDao productDao = daoSession.getProductDao();
        final BasketDao basketDao = daoSession.getBasketDao();

        List<Product> productList = productDao.
                queryBuilder().where(ProductDao.Properties.Id.eq(item.getProductId())).list();
        Product product = productList.get(0);

        holder.basketProduct.setText(product.getName());
        holder.basketPrice.setText(String.valueOf(calculatePriceInBasket(product, item)));
        holder.basketQuantity.setText(String.valueOf(item.getQuantity()));
        holder.basketAddQuantity.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                item.setQuantity(item.getQuantity()+1);
                basketDao.update(item);

                if(context instanceof BasketActivity){
                    ((BasketActivity) context).showSum();
                }

                notifyDataSetChanged();

            }
        });
        holder.basketDeleteQuantity.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(item.getQuantity()>0) {
                    item.setQuantity(item.getQuantity() - 1);
                    basketDao.update(item);
                    if(context instanceof BasketActivity){
                        ((BasketActivity) context).showSum();
                    }
                    notifyDataSetChanged();
                }

            }
        });
//

    }

    private float calculatePriceInBasket(Product product, Basket item) {
        return product.getPrice()*item.getQuantity();
    }


    @Override
    public int getItemCount() {
        return basketList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView basketProduct;
        private TextView basketPrice;
        private TextView basketQuantity;
        private ImageButton basketAddQuantity;
        private ImageButton basketDeleteQuantity;

        public MyViewHolder(View itemView) {
            super(itemView);
            basketProduct = (TextView) itemView.findViewById(R.id.basketProduct);
            basketPrice = (TextView) itemView.findViewById(R.id.basketPrice);
            basketQuantity = (TextView) itemView.findViewById(R.id.basketQuantity);
            basketAddQuantity = (ImageButton) itemView.findViewById(R.id.basketAdd);
            basketDeleteQuantity = (ImageButton) itemView.findViewById(R.id.basketDelete);
        }
    }

}
