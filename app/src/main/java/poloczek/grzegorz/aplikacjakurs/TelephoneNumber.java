package poloczek.grzegorz.aplikacjakurs;

/**
 * Created by RENT on 2017-07-17.
 */

public class TelephoneNumber {

    String telephoneNumber;

    public TelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

}
