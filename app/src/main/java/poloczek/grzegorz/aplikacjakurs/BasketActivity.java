package poloczek.grzegorz.aplikacjakurs;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import com.google.gson.Gson;

import org.greenrobot.greendao.database.Database;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import poloczek.grzegorz.aplikacjakurs.db.Basket;
import poloczek.grzegorz.aplikacjakurs.db.BasketDao;
import poloczek.grzegorz.aplikacjakurs.db.DaoMaster;
import poloczek.grzegorz.aplikacjakurs.db.DaoSession;
import poloczek.grzegorz.aplikacjakurs.db.Product;
import poloczek.grzegorz.aplikacjakurs.db.ProductDao;

public class BasketActivity extends AppCompatActivity {

    private static final int SEND_SMS = 1;
    private BasketAdapter basketAdapter;
    private List<Basket> basketList;
    private RecyclerView myRecyclerView;
    private DaoSession daoSession;
    private ProductDao productDao;
    private BasketDao basketDao;
    private Database database;
    private Toolbar toolbar;
    private TextView basketSum;
    private Context context;
    final String FILE_NAME = "basket.sda";
    private String sendSum;
    private String jsonBasket;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);

//        toolbar = (Toolbar) findViewById(R.id.tool_bar_basket);
//        setSupportActionBar(toolbar);
        basketSum = (TextView) findViewById(R.id.basketSum);
        basketList = new ArrayList<>();
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "users-db");

        Database database = helper.getWritableDb();
        daoSession = new DaoMaster(database).newSession();
        basketDao = daoSession.getBasketDao();

        readData();

        myRecyclerView = (RecyclerView) findViewById(R.id.basketRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration divider = new DividerItemDecoration
                (myRecyclerView.getContext(),linearLayoutManager.getOrientation());

        myRecyclerView.addItemDecoration(divider);

        basketAdapter = new BasketAdapter(basketList, BasketActivity.this);
        myRecyclerView.setAdapter(basketAdapter);

        showSum();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.basket_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


       if (id == R.id.action_save_basket) {
           //dodano permissions dla External Storage

           List<Basket> basketList = basketDao.queryBuilder().list();
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), FILE_NAME);
           Gson gson = new Gson();

           jsonBasket = gson.toJson(basketList);
           try {
               FileOutputStream stream = new FileOutputStream(file);
               stream.write(jsonBasket.getBytes());
               stream.close();
           } catch (Exception e) {
               e.printStackTrace();
           }

           Toast.makeText(getApplicationContext(), "Zapisano do pliku", Toast.LENGTH_LONG).show();
       }


        if (id == R.id.action_load_basket) {
            basketList = readFromMemory();
            basketAdapter = new BasketAdapter(basketList, BasketActivity.this);
            myRecyclerView.setAdapter(basketAdapter);
        }

        if (id == R.id.action_send_sms) {
            openSmsActivity();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SEND_SMS){
            if(resultCode == RESULT_OK){
                Toast.makeText(this, "WYSŁANO SMS", Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED){
                Toast.makeText(this, "NIE WYSŁANO SMS", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void showSum() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "users-db");
        database = helper.getWritableDb();
        daoSession = new DaoMaster(database).newSession();

        productDao = daoSession.getProductDao();
        basketDao = daoSession.getBasketDao();

        List<Basket> basketList = basketDao.queryBuilder().list();
        float sum = 0;

        for(Basket item : basketList){
            int quantity = item.getQuantity();
            List<Product> productList = productDao.queryBuilder().
                    where(ProductDao.Properties.Id.eq(item.getProductId())).list();
            float price = productList.get(0).getPrice();

            sum = sum + ((float)quantity*price);
        }
        basketSum.setText(String.valueOf(sum) + " zł");

        sendSum = String.valueOf(sum);

    }

    public void readData(){
        basketList = basketDao.queryBuilder().list();
    }

    public List<Basket> readFromMemory(){

        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), FILE_NAME);

        StringBuilder stringBuilder = new StringBuilder();

        try {
            FileInputStream fis =  new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            stringBuilder = new StringBuilder();
            String line;
            while((line = br.readLine()) != null){
                stringBuilder.append(line);
            }
            Toast.makeText(getApplicationContext(), "Odczytano z pliku", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Wystąpił błąd", Toast.LENGTH_LONG).show();
        }

        Type type = new TypeToken<List<Basket>>(){}.getType();

        List<Basket> readBasket;

        Gson gson = new Gson();
        readBasket = gson.fromJson(stringBuilder.toString(), type);


        return readBasket;
    }

    public void openSmsActivity(){
        Intent i = new Intent(this, SMSActivity.class);
        i.putExtra("totalPrice", sendSum);
        startActivityForResult(i, SEND_SMS);
    }
}
