package poloczek.grzegorz.aplikacjakurs;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import poloczek.grzegorz.aplikacjakurs.databinding.ActivityKontoBinding;


public class KontoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //nazwa zgodnie z nazwa layoutu, activity_konto + binding;
        ActivityKontoBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_konto);
        User user = (User) getIntent().getSerializableExtra("user");
        binding.setUser(user);
    }
}
