package poloczek.grzegorz.aplikacjakurs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class FragmentLessonActivity extends AppCompatActivity implements NewSecondFragment.MyActivityListener {

    //SPRAWDZ KLASE EVENTBUS OD GREENDAO
    //do przenoszenia informacji pomiedzy activity

    NewFragment newFragment;
    String msg = "PRZEKAZANO Z DRUGIEGO DO PIERWSZEGO :D";
    FragmentManager fm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_lesson);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.firstButton)
    public void loadFirstFragment(){
        loadFragment(new NewFragment());

    }

    @OnClick(R.id.secondButton)
    public void loadSecondFragment(){
        NewSecondFragment newSecondFragment =new NewSecondFragment();
        Bundle bundle = new Bundle();
        bundle.putString("CLASSIFIED", "SECRET CODE: 7898");
        newSecondFragment.setArguments(bundle);
        loadFragment(newSecondFragment);
    }


    public void loadFragment(Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void sendMessage(String msg) {
        NewFragment fragment =  new NewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("tajne", msg);
        fragment.setArguments(bundle);
        loadFragment(fragment);
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount()>0){
            getFragmentManager().popBackStack();
        }else {
            super.onBackPressed();
        }

    }
}
