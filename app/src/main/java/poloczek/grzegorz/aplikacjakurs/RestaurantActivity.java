package poloczek.grzegorz.aplikacjakurs;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.List;

import poloczek.grzegorz.aplikacjakurs.db.DaoMaster;
import poloczek.grzegorz.aplikacjakurs.db.DaoSession;
import poloczek.grzegorz.aplikacjakurs.db.Product;
import poloczek.grzegorz.aplikacjakurs.db.ProductDao;

public class RestaurantActivity extends AppCompatActivity {

    private RestaurantAdapter restaurantAdapter;
    private List<Product> productList;
    private RecyclerView myRecyclerView;
    private DaoSession daoSession;
    private ProductDao productDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
//        toolbar = (Toolbar) findViewById(R.id.tool_bar_basket);
//        setSupportActionBar(toolbar);

        productList = new ArrayList<>();
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "users-db");

        Database database = helper.getWritableDb();
        daoSession = new DaoMaster(database).newSession();
        productDao = daoSession.getProductDao();

        readData();

        myRecyclerView = (RecyclerView) findViewById(R.id.restaurantRecyclerView);
        //manager layoutu - ustawia jeden wiersz pod drugim
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration divider = new DividerItemDecoration
                (myRecyclerView.getContext(),linearLayoutManager.getOrientation());

        myRecyclerView.addItemDecoration(divider);

        //adapter ustala jak ma wygladac/zachowywac sie lista
        restaurantAdapter = new RestaurantAdapter(productList, RestaurantActivity.this);
        myRecyclerView.setAdapter(restaurantAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.restaurant_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_basket) {
            Intent intent = new Intent(RestaurantActivity.this, BasketActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }



    public void addProduct(View view) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(RestaurantActivity.this);
        alertDialog.setTitle("Add new product");

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout.LayoutParams doTextu = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout v = new LinearLayout(this);
        v.setOrientation(LinearLayout.VERTICAL);
        v.setLayoutParams(lp);

        final EditText productName = new EditText(this);
        productName.setHint("Product name");
        productName.setLayoutParams(doTextu);
        v.addView(productName);

        final EditText price = new EditText(this);
        price.setHint("Price");
        price.setInputType(InputType.TYPE_CLASS_NUMBER);
        price.setLayoutParams(doTextu);
        v.addView(price);

        alertDialog.setView(v);

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Product product = new Product();
                product.setName(productName.getText().toString());
                product.setPrice(Float.valueOf(price.getText().toString()));
                productDao.insert(product);
                readData();
                restaurantAdapter = new RestaurantAdapter(productList, getApplicationContext());
                myRecyclerView.setAdapter(restaurantAdapter);
            }
        });

        alertDialog.show();

    }

    public void readData(){
        productList = productDao.queryBuilder().list();
    }

//    public boolean readById(int id){
//
//    }
}
