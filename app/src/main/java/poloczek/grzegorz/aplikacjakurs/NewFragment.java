package poloczek.grzegorz.aplikacjakurs;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewFragment extends Fragment {

    TextView textView;

    public NewFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_new, container, false);
        textView = (TextView) v.findViewById(R.id.firstFragmentTextView);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String msg = "";
        Bundle bundle = getArguments();
        if(bundle!=null) {
            msg = bundle.getString("tajne");
        }
        if(!msg.isEmpty()){
            textView.setText(msg);
        }

    }
}
