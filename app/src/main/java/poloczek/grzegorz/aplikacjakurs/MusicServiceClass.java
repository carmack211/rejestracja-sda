package poloczek.grzegorz.aplikacjakurs;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import java.io.IOException;

/**
 * Created by Grzegorz Poloczek on 2017-06-12.
 */

public class MusicServiceClass extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private MediaPlayer mp;

    @Override
    public void onCreate(){
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        mp = new MediaPlayer();
        try {
            mp.setDataSource(getApplicationContext(), Uri.parse("android.resource://poloczek.grzegorz.aplikacjakurs/"+ R.raw.need_for_speed_2015));
        } catch (IOException e) {
            e.printStackTrace();
        }
        mp.setLooping(true);
        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer player) {
                player.start();
            }
        });
        mp.prepareAsync();

        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        mp.reset();
        mp.release();
    }
}