package poloczek.grzegorz.aplikacjakurs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import poloczek.grzegorz.aplikacjakurs.data.DbQueries;
import poloczek.grzegorz.aplikacjakurs.data.ToDoModel;

/**
 * Created by Grzegorz on 2017-06-16.
 */

public class ToDoListAdapter extends RecyclerView.Adapter<ToDoListAdapter.MyViewHolder> {
    private List<ToDoModel> toDoList;
    private DbQueries dbQueries;
    private Context context;

    public ToDoListAdapter(List<ToDoModel> toDoList, Context context) {

        this.context = context;
        this.toDoList = toDoList;
        dbQueries = new DbQueries(context);
        dbQueries.save();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.line_todo,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ToDoModel item = toDoList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewChoice)context).onViewChoice(item.getContentTask());
            }
        });

        holder.toDoTask.setText(item.getToDoTask());
        holder.editTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                alertDialog.setTitle("Edit task");
                LinearLayout layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);
                final EditText editText = new EditText(v.getContext());
                final EditText secondEditText = new EditText(v.getContext());
                layout.addView(editText);
                layout.addView(secondEditText);
                editText.setText(item.getToDoTask());
                secondEditText.setText(item.getContentTask());
                alertDialog.setView(layout);

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dbQueries.editToDoTask(editText.getText().toString(), String.valueOf(item.getId()), secondEditText.getText().toString());
                        toDoList = dbQueries.returnToDoModel();
                        notifyDataSetChanged();

                    }
                });

                alertDialog.show();
            }
        });

        holder.deleteTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbQueries.deleteToDoTask(String.valueOf(item.getId()));
                toDoList = dbQueries.returnToDoModel();
                notifyDataSetChanged();
//                notifyItemRangeRemoved(0, listaZadan.size());
            }
        });

    }

    @Override
    public int getItemCount() {
        return toDoList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView toDoTask;
        private ImageButton editTask;
        private ImageButton deleteTask;

        public MyViewHolder(View itemView) {
            super(itemView);
            toDoTask = (TextView) itemView.findViewById(R.id.toDoTextLine);
            editTask = (ImageButton) itemView.findViewById(R.id.toDoEdit);
            deleteTask = (ImageButton) itemView.findViewById(R.id.toDoDelete);
        }
    }

    public static String returnImageURI(int link){
        String imageUri = "drawable://" + link;
        return imageUri;
    }



}
