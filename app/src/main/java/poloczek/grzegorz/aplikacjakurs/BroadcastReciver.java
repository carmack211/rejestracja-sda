package poloczek.grzegorz.aplikacjakurs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;

/**
 * Created by Grzegorz on 2017-06-09.
 */

public class BroadcastReciver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED")) {
            Toast.makeText(context, "Telefon został podłączony do ładowarki", Toast.LENGTH_LONG).show();

        }

               else if(intent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
                    Toast.makeText(context, "Telefon został odłączony od ładowarki", Toast.LENGTH_LONG).show();
                }
    }
}
