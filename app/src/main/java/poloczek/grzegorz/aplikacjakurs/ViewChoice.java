package poloczek.grzegorz.aplikacjakurs;

/**
 * Created by Grzegorz on 2017-07-24.
 */

public interface ViewChoice {
    public void onViewChoice(String text);
}
