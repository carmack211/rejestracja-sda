package com.example;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Property;
import org.greenrobot.greendao.generator.Schema;

public class MyGenerator {
    public static void main(String[] args) {
        Schema schema = new Schema(2, "poloczek.grzegorz.aplikacjakurs.db");
        schema.enableKeepSectionsByDefault();

        addTables(schema);

        try {
            new DaoGenerator().generateAll(schema, "./app/src/main/java");
        }catch ( Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTables(Schema schema) {
        //addProductsEntity(schema);
        //addOrdersEntity(schema);

        Entity product = schema.addEntity("Product");
        product.addIdProperty().primaryKey().autoincrement();
        product.addStringProperty("name");
        product.addFloatProperty("price");

        Entity basket = schema.addEntity("Basket");
        basket.addIdProperty().primaryKey().autoincrement();
        Property productIdProperty = basket.addLongProperty("productId").getProperty();
        basket.addToOne(product, productIdProperty);
        basket.addIntProperty("quantity");

        Entity order = schema.addEntity("Order");
        order.addIdProperty().primaryKey().autoincrement();
        order.addFloatProperty("sum");
        order.addDateProperty("date");
        order.addStringProperty("name");
        order.addStringProperty("surname");
        order.addStringProperty("street");
        order.addStringProperty("zipcode");
        order.addStringProperty("city");

        Entity orderDetails = schema.addEntity("OrderDetails");
        orderDetails.addIdProperty().primaryKey().autoincrement();
        orderDetails.addIntProperty("quantity");

        Property productIdPropertyForOrderDetails = orderDetails.addLongProperty("productId").getProperty();
        orderDetails.addToOne(product, productIdPropertyForOrderDetails);
        Property orderIdPropertyForOrderDetails = orderDetails.addLongProperty("orderId").getProperty();
        orderDetails.addToOne(order, orderIdPropertyForOrderDetails);

    }

//    private static Entity addProductsEntity(final Schema schema) {
//        Entity product = schema.addEntity("Product");
//        product.addIdProperty().primaryKey().autoincrement();
//        product.addStringProperty("name");
//        product.addFloatProperty("price");
//
//        return product;
//    }
//
//    private static Entity addOrdersEntity(final Schema schema) {
//        Entity order = schema.addEntity("Order");
//        order.addIdProperty().primaryKey().autoincrement();
//        order.addStringProperty("product");
//        order.addFloatProperty("sum");
//        order.addDateProperty("date");
//
//        return order;
//    }

}
